#!/bin/sh
#Verificação de Servidor:
clear

echo "- Digite a informação desejada: -"
echo - [p] Processador.
echo - [m] Memória.
echo - [c] Uso da CPU.
echo - [s] Posição de HDD/SSD na MOBO
echo "Opção:"
read informacao

if [ "$informacao" = "p" ]
then
echo "-----------------------------------------------------"
echo "|             Informação do Processador             |"
echo "-----------------------------------------------------"
echo "-Especificações-"
echo
lscpu | grep fornecedor:
lscpu | grep modelo:
lscpu | grep CPU:
echo
echo "-Numero de Nucleos-"
echo 
lscpu | grep CPU'(s)':
lscpu | grep núcleo:
lscpu | grep soquete:
echo 
echo "-Clock da CPU-"
echo
lscpu | grep -i mhz
echo
echo "-Cache da CPU-"
echo
lscpu | grep L3:
echo
echo "----------------------------------------------------"
fi

if [ "$informacao" = "m" ]
then
echo "-----------------------------------------------------"
echo "|             Informação da Memória               |"
echo "-----------------------------------------------------"
echo
sudo dmidecode -t 16 | grep SMBIOS
echo
echo "-Capacidade Maxima de Memoria-"
echo
sudo dmidecode -t 16 | grep Maximum | cut -d":" -f2
echo
echo "-Slots Disponíveis-"
echo
sudo dmidecode -t 16 | grep Number | cut -d":" -f2
echo
echo "-Tamanho da Memória-"
echo
sudo dmidecode -t 17 | grep Size: | cut -d":" -f2
echo
echo "-Uso da Memória-"
echo
free -m | grep total | cut -b 13-31
free -m | grep Mem.: | cut -b 13-31
echo
echo "-----------------------------------------------------"
fi

if [ "$informacao" = "c" ]
then
echo "---------------------------------"
echo "           Uso da CPU            "
echo "---------------------------------"
echo
top
echo
fi

if [ "$informacao" = "s" ]
then
echo "---------------------------------"
echo "     Posição do HDD/SSD MOBO     "
echo "---------------------------------"
echo 
sudo lsscsi
echo
echo "-----------------------------------------------------"
fi

